import { Component, h, Prop } from '@stencil/core';
import { CoolContent } from './functional-components/CoolContent';

@Component( {
  tag: 'cool-card-functional',
  styleUrl: 'cool-card-functional.css',
  shadow: true
})
export class CoolCardFunctional {

  @Prop() firstname: string = '';
  @Prop() lastname: string = '';
  @Prop() image: string = '';
  @Prop() description: string = '';

  render() {
    return (
      <CoolContent
        firstname={this.firstname}
        lastname={this.lastname}
        image={this.image}
        description={this.description}
      />
    )
  }
};