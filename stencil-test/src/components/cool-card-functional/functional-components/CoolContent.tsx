import { FunctionalComponent, h } from '@stencil/core';

interface CoolProps {
  image: string;
  firstname: string;
  lastname: string;
  description: string;
}

export const CoolContent: FunctionalComponent<CoolProps> = (props) => {
  const {
    image,
    firstname,
    lastname,
    description
  } = props;
  
  return (
    <div class='cool-card-f-container'>
      <div class="cool-card-f-image">
        <img src={image} />
      </div>
      <div class="cool-card-f-text-content">
        <h3 class="cool-card-f-names">
          {firstname} {lastname}
        </h3>
        <div class="cool-card-f-description">
          {description}
        </div>
      </div>
    </div>
  )
}