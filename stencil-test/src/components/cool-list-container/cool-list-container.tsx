import { Component, h } from '@stencil/core';

@Component( {
  tag: 'cool-list-container',
  styleUrl: 'cool-list-container.css',
  shadow: true
})
export class CoolCard {

  render() {
    return (
      <div class='cool-list-container'>
        <slot></slot>
      </div>
    )
  }
};