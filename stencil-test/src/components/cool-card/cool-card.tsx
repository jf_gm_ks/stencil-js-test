import { Component, h, Prop } from '@stencil/core';

@Component( {
  tag: 'cool-card',
  styleUrl: 'cool-card.css',
  shadow: true
})
export class CoolCard {

  @Prop() firstname: string = '';
  @Prop() lastname: string = '';
  @Prop() image: string = '';
  @Prop() description: string = '';

  render() {
    return (
      <div class='cool-card-container'>
        <div class="cool-card-image">
          <img src={this.image} />
        </div>
        <div class="cool-card-text-content">
          <h3 class="cool-card-names">
            {this.firstname} {this.lastname}
          </h3>
          <div class="cool-card-description">
            {this.description}
          </div>
        </div>
      </div>
    )
  }
};